package fr.ubx.poo.ugarden.go.personage;

import java.util.TimerTask;
public class regenEnergy extends TimerTask{
    private Gardener gardener;
    regenEnergy(Gardener gardener) {
        this.gardener = gardener;
    }
    public void run() {
        gardener.addEnergy();
    }
}