/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ugarden.go.personage;

import fr.ubx.poo.ugarden.game.Direction;
import fr.ubx.poo.ugarden.game.Game;
import fr.ubx.poo.ugarden.game.Position;
import fr.ubx.poo.ugarden.go.GameObject;
import fr.ubx.poo.ugarden.go.Movable;
import fr.ubx.poo.ugarden.go.TakeVisitor;
import fr.ubx.poo.ugarden.go.WalkVisitor;
import fr.ubx.poo.ugarden.go.bonus.*;
import fr.ubx.poo.ugarden.go.decor.Decor;
import fr.ubx.poo.ugarden.go.decor.Flower;
import fr.ubx.poo.ugarden.launcher.MapEntity;
import java.util.Timer;


public class Gardener extends GameObject implements Movable, TakeVisitor, WalkVisitor {

    private int energy;
    private int diseaseLevel;
    private Direction direction;
    private boolean moveRequested = false;
    private Timer timer;

    private Timer diseaseTimer;
    private int nbOfKey;
    public boolean getHerrison;
    private int nbOfInsectiside;

    public Gardener(Game game, Position position) {

        super(game, position);
        this.direction = Direction.DOWN;
        this.energy = game.configuration().gardenerEnergy();
        this.diseaseLevel = 0;
        this.timer = new Timer();
        this.diseaseTimer = new Timer();
        timer.schedule(new regenEnergy(this),1000*game.configuration().energyRecoverDuration(),1000*game.configuration().energyRecoverDuration());
        this.nbOfKey = 0;
        this.nbOfInsectiside = 0;
    }
    public int getNbOfInsectiside(){return nbOfInsectiside;}
    public int getNbOffKey(){return  nbOfKey;}
    public int getDiseaseLevel() {
        return diseaseLevel;
    }

    @Override
    public void take(Key key) {
// TODO
        System.out.println("I am taking the key, I should do something ...");
        this.nbOfKey += 1;
    }

    @Override
    public void take(PoisonApple apple){

        this.diseaseLevel += 1;
        diseaseTimer.schedule(new diseaseTime(this), 1000*game.configuration().diseaseDuration());
    }

    public void energyRecovery(){
        this.diseaseLevel = this.diseaseLevel - 1;
    }
    public void take(Hornet hornet){
        if (this.nbOfInsectiside == 0){
            hurt(20);
        }
        else{
            this.nbOfInsectiside -= 1;
        }
    }
    public void take(Insecticide insecticide){
        this.nbOfInsectiside += 1;
    }
    public void take(Apple apple){
        this.diseaseLevel = 0;
    }
    public void take(Herrison herrison){
        this.getHerrison = true;
    }
    public boolean canWalkOn(Flower flower){
        return false;
    }

    public int getEnergy() {
        return this.energy;
    }
    public void addEnergy(){
        if(this.energy < game.configuration().gardenerEnergy()) {
            this.energy += 1;
        }
    }

    public void requestMove(Direction direction) {
        if (direction != this.direction) {
            this.direction = direction;
            setModified(true);
        }
        moveRequested = true;
    }

    @Override
    public final boolean canMove(Direction direction) {
        Position nextPos = direction.nextPosition(getPosition());
        if (nextPos.x() < 0 || nextPos.x() >= this.game.world().getGrid().width() || nextPos.y() < 0 || nextPos.y() >= this.game.world().getGrid().height()) { return false;}
        Decor next = game.world().getGrid().get(nextPos);
        if(next != null){
            return next.walkableBy(this);
        }
        return true;
    }

    @Override
    public void doMove(Direction direction) {
        // Restart the timer
        Position nextPos = direction.nextPosition(getPosition());
        Decor next = game.world().getGrid().get(nextPos);
        setPosition(nextPos);
        if (next != null)
            next.takenBy(this);
        timer.cancel();
        timer = new Timer();
        timer.schedule(new regenEnergy(this),1000*game.configuration().energyRecoverDuration(),1000*game.configuration().energyRecoverDuration());
        energy -= next.energyConsumptionWalk() + this.diseaseLevel;
    }

    public void update(long now) {
        if (moveRequested) {
            if (canMove(direction)) {
                doMove(direction);
            }
        }
        moveRequested = false;
    }

    public void hurt(int damage) {
        this.energy = this.energy - damage;
    }



    public Direction getDirection() {
        return direction;
    }


}
