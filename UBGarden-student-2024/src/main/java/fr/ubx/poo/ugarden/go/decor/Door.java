package fr.ubx.poo.ugarden.go.decor;

import fr.ubx.poo.ugarden.game.Position;
import fr.ubx.poo.ugarden.go.personage.Gardener;
import fr.ubx.poo.ugarden.go.personage.Hornet;

public class Door extends Decor {
    private boolean opened;
    public Door(Position position,boolean opened) {

        super(position);
        this.opened = opened;
    }

    @Override
    public boolean walkableBy(Gardener gardener) {
        return opened;}
    public boolean walkableBy(Hornet hornet) {
        return opened;}
}