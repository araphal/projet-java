package fr.ubx.poo.ugarden.launcher;

import fr.ubx.poo.ugarden.game.*;

import java.io.*;
import java.util.Properties;
import java.lang.String;
import java.lang.Integer;
public class GameLauncher {

    private int levels;
    private MapLevel[] mapLevels;

    private GameLauncher() {
    }

    public static GameLauncher getInstance() {
        return LoadSingleton.INSTANCE;
    }

    private int integerProperty(Properties properties, String name, int defaultValue) {
        return Integer.parseInt(properties.getProperty(name, Integer.toString(defaultValue)));
    }

    private boolean booleanProperty(Properties properties, String name, boolean defaultValue) {
        return Boolean.parseBoolean(properties.getProperty(name, Boolean.toString(defaultValue)));
    }

    private Configuration getConfiguration(Properties properties) {

        // Load parameters
        int hornetMoveFrequency = integerProperty(properties, "hornetMoveFrequency", 1);
        int gardenerEnergy = integerProperty(properties, "gardenerEnergy", 100);
        int energyBoost = integerProperty(properties, "energyBoost", 50);
        int energyRecoverDuration = integerProperty(properties, "energyRecoverDuration", 1);
        int diseaseDuration = integerProperty(properties, "diseaseDuration", 5);

        return new Configuration(gardenerEnergy, energyBoost, hornetMoveFrequency, energyRecoverDuration, diseaseDuration);
    }

    public Game load(File file) {
        Properties file_props = new Properties();
        try(FileInputStream in = new FileInputStream(file)) {
            file_props.load(in);
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
        int nb_levels = Integer.parseInt(file_props.getProperty("level"));
        String[] levels = new String[nb_levels];
        for(int i = 1; i <= nb_levels;i++){
            levels[i-1] = file_props.getProperty("level" + Integer.toString(i));
        }
        if(file_props.getProperty("compression") == "true"){
            //compress the levels here
        }
        //Get width and height here
        World world = new World(nb_levels);
        Configuration configuration = getConfiguration(new Properties());
        Game game = null;
        //for(int i = 1; i <= nb_levels; i++){
            //MapLevel mapLevel = new mapLevel(width,height);
            //Fill the mapLevel
            //if(i == 1){
                //Position gardenerPosition = mapLevel.getGardenerPosition();
                //if (gardenerPosition == null)
                    //throw new RuntimeException("Gardener not found");
                //game = new Game(world, configuration, gardenerPosition);
            //}
            //Map level = new Level(game,i,mapLevel);
            //world.put(i,level)
        //}
        return game;
    }

    public Game load() {
        Properties emptyConfig = new Properties();
        MapLevel mapLevel = new MapLevelDefault();
        Position gardenerPosition = mapLevel.getGardenerPosition();
        if (gardenerPosition == null)
            throw new RuntimeException("Gardener not found");
        Configuration configuration = getConfiguration(emptyConfig);
        World world = new World(1);
        Game game = new Game(world, configuration, gardenerPosition);
        Map level = new Level(game, 1, mapLevel);
        world.put(1, level);
        return game;
    }

    private static class LoadSingleton {
        static final GameLauncher INSTANCE = new GameLauncher();
    }

}
