package fr.ubx.poo.ugarden.go.personage;

import java.util.TimerTask;

public class diseaseTime extends TimerTask {
    private Gardener gardener;
    diseaseTime(Gardener gardener) {
        this.gardener = gardener;
    }
    public void run(){
        if (gardener.getDiseaseLevel() != 0){
            gardener.energyRecovery();
        }
    }
}
