package fr.ubx.poo.ugarden.go.personage;

import fr.ubx.poo.ugarden.game.Direction;
import fr.ubx.poo.ugarden.game.Game;
import fr.ubx.poo.ugarden.game.Position;
import fr.ubx.poo.ugarden.go.GameObject;
import fr.ubx.poo.ugarden.go.Movable;
import fr.ubx.poo.ugarden.go.Takeable;
import fr.ubx.poo.ugarden.go.WalkVisitor;
import fr.ubx.poo.ugarden.go.TakeVisitor;
import fr.ubx.poo.ugarden.go.decor.Decor;
import fr.ubx.poo.ugarden.go.bonus.Bonus;
import java.util.Timer;
import javax.swing.*;

public class Hornet extends GameObject implements Movable,TakeVisitor,WalkVisitor{


    private Direction direction;




    private boolean moveRequested = false;
    private Game game;
    private Timer moveTimer;

    public Hornet (Position position,Game game){
        super(position);
        this.direction = Direction.LEFT;
        this.game = game;
        this.moveTimer = new Timer();
        moveTimer.schedule(new movementFrequency(this),1000*game.configuration().hornetMoveFrequency(),1000*game.configuration().hornetMoveFrequency());
    }

    public Direction getDirection() {
        return direction;
    }
    public void setMoveRequested(boolean moveRequested) {
        this.moveRequested = moveRequested;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }


    @Override
    public boolean canMove(Direction direction){
        Position nextPos = direction.nextPosition(getPosition());
        if (nextPos.x() < 0 || nextPos.x() >= this.game.world().getGrid().width() || nextPos.y() < 0 || nextPos.y() >= this.game.world().getGrid().height()) { return false;}
        Decor next = game.world().getGrid().get(nextPos);
        if(next != null){
            return next.walkableBy(this);
        }
        return true;
    }
    @Override
    public void doMove(Direction direction){
        Position nextPos = direction.nextPosition(getPosition());
        Decor next = game.world().getGrid().get(nextPos);
        setPosition(nextPos);
    }


    public void update(long now) {
        if (moveRequested) {
            if (canMove(direction)) {
                doMove(direction);
            }
        }
        moveRequested = false;
    }

}






