package fr.ubx.poo.ugarden.go.personage;



import java.util.TimerTask;



public class movementFrequency extends TimerTask {
    private Hornet hornet;

    movementFrequency(Hornet hornet){this.hornet = hornet;}
    public void run(){
        hornet.setMoveRequested(true);
        hornet.setDirection(hornet.getDirection().random());
    }
}
