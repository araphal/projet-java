package fr.ubx.poo.ugarden.go.bonus;

import fr.ubx.poo.ugarden.game.Position;
import fr.ubx.poo.ugarden.go.decor.Decor;
import java.util.Timer;
import fr.ubx.poo.ugarden.game.Game;
import fr.ubx.poo.ugarden.go.personage.Gardener;
public class Nest extends Bonus {
    Timer timer;
    Game game;
    public Nest (Position position, Decor decor,Game game){
        super(position, decor);
        this.game = game;
        timer = new Timer();
        timer.schedule(new generateHornet(position,game),10000,10000);
    }

}
