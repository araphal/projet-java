package fr.ubx.poo.ugarden.go;

import fr.ubx.poo.ugarden.go.bonus.Apple;
import fr.ubx.poo.ugarden.go.bonus.Key;
import fr.ubx.poo.ugarden.go.bonus.PoisonApple;

public interface TakeVisitor {

    // Key
    default void take(Key key) { return;
    }
    default void take(Apple apple){
    }

    default void take(PoisonApple poisonapple){
    }

}
