package fr.ubx.poo.ugarden.game;

import fr.ubx.poo.ugarden.go.personage.Gardener;

import fr.ubx.poo.ugarden.go.personage.Hornet;



import java.util.ArrayList;
import fr.ubx.poo.ugarden.go.personage.Hornet;
import fr.ubx.poo.ugarden.go.bonus.Insecticide;

public class Game {

    private final Configuration configuration;
    private final World world;
    private final Gardener gardener;


    private ArrayList hornets = new ArrayList();
    private ArrayList insecticides = new ArrayList();

    private boolean switchLevelRequested = false;
    private int switchLevel;
    public Game(World world, Configuration configuration, Position gardenerPosition) {
        this.configuration = configuration;
        this.world = world;
        gardener = new Gardener(this, gardenerPosition);
    }

    public Configuration configuration() {
        return configuration;
    }

    public Gardener getGardener() {
        return this.gardener;
    }

    public World world() {
        return world;
    }

    public boolean isSwitchLevelRequested() {
        return switchLevelRequested;
    }

    public int getSwitchLevel() {
        return switchLevel;
    }

    public void requestSwitchLevel(int level) {
        this.switchLevel = level;
        switchLevelRequested = true;
    }

    public void clearSwitchLevel() {
        switchLevelRequested = false;
    }

    public void addHornet(Hornet hornet){
        hornets.add(hornet);
    }
    public ArrayList getHornets(){return this.hornets;}
    public void removeHornet(Hornet hornet){
        hornets.remove(hornet);
    }

    public void addInsecticide(Insecticide insecticide){
        insecticides.add(insecticide);
    }
    public ArrayList getInsecticides(){return this.insecticides;}
    public void removeInsecticide(Insecticide insecticide){
        insecticides.remove(insecticide);
    }
}
