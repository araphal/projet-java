package fr.ubx.poo.ugarden.go.bonus;

import java.util.TimerTask;
import fr.ubx.poo.ugarden.game.Game;
import fr.ubx.poo.ugarden.game.Position;
import fr.ubx.poo.ugarden.go.decor.Decor;
import fr.ubx.poo.ugarden.go.personage.Hornet;
import fr.ubx.poo.ugarden.launcher.MapEntity;
import fr.ubx.poo.ugarden.go.decor.ground.Grass;
import java.util.Random;

public class generateHornet extends TimerTask{
    private Position position;
    private Game game;
    generateHornet(Position position,Game game){
        this.position = position;
        this.game = game;
    }
    public void run() {
        Hornet hornet = new Hornet(position,game);
        game.addHornet(hornet);
        Random randomWidth = new Random();
        int width = randomWidth.nextInt(this.game.world().getGrid().width());
        Random randomHeight = new Random();
        int height = randomHeight.nextInt(this.game.world().getGrid().height());
        Position pos = new Position(game.world().currentLevel(), width, height);
        Decor decor = game.world().getGrid().get(pos);
        while(!(decor instanceof Grass) || decor.getBonus() != null){
            width = randomWidth.nextInt(this.game.world().getGrid().width());
            height = randomHeight.nextInt(this.game.world().getGrid().height());
            pos = new Position(game.world().currentLevel(), width, height);
            decor = game.world().getGrid().get(pos);
        }
        Insecticide insecticide = new Insecticide(pos, decor);
        decor.setBonus(insecticide);
        this.game.world().getGrid().set(pos, decor);
        this.game.addInsecticide(insecticide);
    }
}