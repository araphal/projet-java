/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ugarden.view;

import fr.ubx.poo.ugarden.go.GameObject;
import fr.ubx.poo.ugarden.go.bonus.*;
import fr.ubx.poo.ugarden.go.decor.Flower;
import fr.ubx.poo.ugarden.go.decor.Tree;
import fr.ubx.poo.ugarden.go.decor.ground.Carrot;
import fr.ubx.poo.ugarden.go.decor.ground.Earth;
import fr.ubx.poo.ugarden.go.decor.ground.Grass;
import fr.ubx.poo.ugarden.go.personage.Herrison;
import javafx.scene.layout.Pane;

import static fr.ubx.poo.ugarden.view.ImageResource.*;


public final class SpriteFactory {

    public static Sprite create(Pane layer, GameObject gameObject) {
        ImageResourceFactory factory = ImageResourceFactory.getInstance();
        if (gameObject instanceof Grass)
            return new Sprite(layer, factory.get(GRASS), gameObject);
        if (gameObject instanceof Tree)
            return new Sprite(layer, factory.get(TREE), gameObject);
        if (gameObject instanceof Key)
            return new Sprite(layer, factory.get(KEY), gameObject);
        if (gameObject instanceof Herrison)
            return new Sprite(layer,factory.get(HEDGEHOG), gameObject);
        if (gameObject instanceof Earth)
            return new Sprite(layer, factory.get(LAND), gameObject);
        if (gameObject instanceof Carrot)
            return new Sprite(layer, factory.get(CARROTS), gameObject);
        if (gameObject instanceof Apple)
            return new Sprite(layer, factory.get(APPLE), gameObject);
        if (gameObject instanceof Flower)
            return new Sprite(layer, factory.get(FLOWERS), gameObject);
        if (gameObject instanceof PoisonApple)
            return new Sprite(layer, factory.get(POISONED_APPLE), gameObject);
        if (gameObject instanceof Nest){
            return new Sprite(layer, factory.get(NEST), gameObject);
        }
        if (gameObject instanceof Insecticide){
            return new Sprite(layer, factory.get(INSECTICIDE), gameObject);
        }
        throw new RuntimeException("Unsupported sprite for decor " + gameObject);
    }
}
