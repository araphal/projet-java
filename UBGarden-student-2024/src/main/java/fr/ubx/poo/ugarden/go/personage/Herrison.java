package fr.ubx.poo.ugarden.go.personage;

import fr.ubx.poo.ugarden.game.Direction;
import fr.ubx.poo.ugarden.game.Game;
import fr.ubx.poo.ugarden.game.Position;
import fr.ubx.poo.ugarden.go.GameObject;
import fr.ubx.poo.ugarden.go.bonus.Bonus;
import fr.ubx.poo.ugarden.go.decor.Decor;
import javafx.geometry.Pos;

public class Herrison extends Bonus {

    public Herrison (Position position, Decor decor){super(position, decor);}

    public void takenBy(Gardener gardener) {
        gardener.take(this);
        this.remove();
    }
}
